export const result = {
  getPlayer(name) {
    // returns a new player instance with the provided name
  },
  getPlaylist(name) {
    // returns a new playlist instance with the provided name
  },
  getAudio(title, author, length) {
    // returns a new audio instance with the provided title, author and length
  },
  getVideo(title, author, imdbRating) {
    // returns a new video instance with the provided title, author and imdbRating
  },
};
